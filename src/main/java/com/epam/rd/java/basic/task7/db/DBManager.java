package com.epam.rd.java.basic.task7.db;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.mysql.cj.xdevapi.Result;


public class DBManager {

	Connection conn;
	Statement statement;

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		instance = new DBManager();
		return instance;
	}

	private Properties loadProperties(){
		Properties p = new Properties();
		try {
			FileReader reader = new FileReader("app.properties");
			p.load(reader);
			return p;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return p;
	}

	private void connectionDB(){
		try {
			String url = loadProperties().getProperty("connection.url");
			//String url = "jdbc:sqlite:F:\\Programming\\SQLiteStudio\\DB\\EpamTask.db";
			conn = DriverManager.getConnection(url);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	private DBManager() {
		this.connectionDB();
//		try {
//
//			File query = new File("F:\\Важно\\Epam_Learn\\task-07\\sql\\db-create.sql");
//			String sql = Files.readString(query.toPath(), Charset.defaultCharset());
//			statement = conn.createStatement();
//			statement.executeUpdate(sql);
//
//			statement.close();
//
//		} catch (SQLException | IOException e) {
//			System.out.println(e.getMessage());
//		}
	}

	public List<User> findAllUsers() throws DBException {
		String sql = "SELECT * FROM users";
		ResultSet res;
		List ll = new ArrayList<User>();
		try {
			Statement statement = conn.createStatement();
			res = statement.executeQuery(sql);
			while (res.next()) {
				int i = res.getInt("id");
				String str = res.getString("login");

				//Assuming you have a user object
				User user = new User(i, str);

				ll.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ll;
	}

	public boolean insertUser(User user) throws DBException {
		String sql = "INSERT INTO users (login) VALUES ('" + user.getLogin() + "')";
		PreparedStatement statement;
		try {
			statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
			if (generatedKeys.next()) {
				user.setId(generatedKeys.getInt(1));
			} else {
				throw new SQLException("Creating user failed, no ID obtained.");
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		String sql = "";
		for (User user:users) {
			sql += "delete from users where id =" + user.getId() + "";
		}
		try {
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public User getUser(String login) throws DBException {
		ResultSet res;
		String sql = "Select * from users where login = '" + login + "'";
		try {
			Statement statement = conn.createStatement();
			res = statement.executeQuery(sql);
			if(res.next()) return new User(res.getInt(1), res.getString(2));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		ResultSet res;
		String sql = "Select * from teams where name = '" + name + "'";
		try {
			Statement statement = conn.createStatement();
			res = statement.executeQuery(sql);
			if(res.next()) return new Team(res.getInt(1), res.getString(2));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		String sql = "SELECT * FROM teams";
		ResultSet res;
		List ll = new ArrayList<Team>();
		try {
			Statement statement = conn.createStatement();
			res = statement.executeQuery(sql);
			while (res.next()) {
				int i = res.getInt("id");
				String str = res.getString("name");

				//Assuming you have a user object
				Team team = new Team(i, str);

				ll.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ll;
	}

	public boolean insertTeam(Team team) throws DBException {
		String sql = "INSERT INTO teams (name) VALUES ('" + team.getName() + "')";
		PreparedStatement statement;
		try {
			statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.execute();
		} catch (SQLException e) {
			return false;
		}
		try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
			if (generatedKeys.next()) {
				team.setId(generatedKeys.getInt(1));
			} else {
				throw new SQLException("Creating user failed, no ID obtained.");
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String[] sql = new String[teams.length];
		if(user == null) return false;
		for (int i = 0; i < teams.length; i++) {
			if(teams[i] == null) continue;
			sql[i] = "INSERT INTO users_teams (user_id, team_id) VALUES (" + user.getId() + ", " + teams[i].getId() +")";
		}

		try {
			Statement statement = conn.createStatement();
			conn.setAutoCommit(false);

			for (int i = 0; i < sql.length; i++) {
				statement.executeUpdate(sql[i]);
			}
			conn.commit();
			conn.setAutoCommit(true);
			return true;
		} catch (SQLException e) {
			try {
				conn.rollback();
				throw new DBException("Transaction Error!", e);
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
			return false;
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		String sql = "SELECT t.name " +
				"FROM users u, teams t, users_teams ut " +
				"WHERE u.id=ut.user_id AND t.id=ut.team_id AND u.login = '"+user.getLogin()+"'";// +
		//"group by u.id";
		ResultSet res;
		List ll = new ArrayList<Team>();
		try {
			Statement statement = conn.createStatement();
			//PreparedStatement statement1 = conn.prepareStatement(sql);
			res = statement.executeQuery(sql);
			//res = statement1.executeQuery();
			while (res.next()) {
				String str = res.getString(1);

				//List<String> tempList = new ArrayList<String>(Arrays.asList(str.split(" ")));
				List<Team> teamList = this.findAllTeams();
				for(Team team: teamList){
					if(team.getName().toString().equals(res.getString(1))){
						ll.add(team);
						break;
					}
				}
//				tempList.forEach(string ->{
//					for (Team team: teamList) {
//						if(string.equals(team.getName())) ll.add(team);
//					}
//				});
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ll;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String sql = "delete from teams where name ='" + team.getName() + "'";
		try {
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		String sql = "UPDATE teams SET name= '"+ team.getName() +"' WHERE id = " + team.getId() + "";
		try {
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	private ResultSet find(String sql){
		ResultSet res;
		try {
			Statement statement = conn.createStatement();
			res = statement.executeQuery(sql);
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
